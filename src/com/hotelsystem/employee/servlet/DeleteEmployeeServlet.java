package com.hotelsystem.employee.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.employee.service.EmployeeService;

@WebServlet("/hotel/employee/delete")
public class DeleteEmployeeServlet extends HttpServlet{

	private EmployeeService allemployeeService=new EmployeeService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	   String workid=req.getParameter("empWorkid");
	   try {
		allemployeeService.deleteEmployee(workid);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   resp.sendRedirect("/hotelmanagesystem/hotel/index");
	   
	}
  
}
