package com.hotelsystem.employee.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Customer;
import com.hotelsystem.common.entity.EmployeeProperties;
import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.customer.service.CustomerService;
import com.hotelsystem.employee.service.EmployeeService;
import com.hotelsystem.park.service.ParkService;
import com.hotelsystem.room.service.RoomService;
@WebServlet("/hotel/singlequery")
public class SingalQueryEmployeeSevlet extends HttpServlet{
    private EmployeeService employeeService=new EmployeeService();
    private RoomService roomService=new RoomService();
	private CustomerService customerService=new CustomerService();
	private ParkService parkService=new ParkService();
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("text/html;charset=UTF-8");
		
		List<Room> rooms=roomService.getRooms();
		List<Customer> customers=customerService.getCustomers();
		List<Park> parks=parkService.getCars();
		
		req.setAttribute("rooms", rooms);
		req.setAttribute("customers", customers);
		req.setAttribute("park", parks);
		
		String str=req.getParameter("empWorkid");
		if(str==null){
			str="";
		}
		if(str!=""){
			String empWorkid=req.getParameter("empWorkid");
			
			try {
			List<EmployeeProperties>employeeProperties = employeeService.singalEmployee(empWorkid);
			req.setAttribute("employeeProperties", employeeProperties);
			req.getRequestDispatcher("/WEB-INF/views/main/index.jsp").forward(req, resp);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


}
