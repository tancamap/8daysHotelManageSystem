package com.hotelsystem.employee.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.EmployeeProperties;
import com.hotelsystem.employee.service.EmployeeService;
@WebServlet("/hotel/employee/insert")
public class InsertEmployeeServlet extends HttpServlet{
    private EmployeeService employeeService=new EmployeeService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/views/employee/InsertEmployee.jsp").forward(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String empWorkid=req.getParameter("empWorkid");
		String empName=req.getParameter("empName");
		String empSex=req.getParameter("empSex");
		String empId=req.getParameter("empId");
		String empPos=req.getParameter("empPos");
		String empSer=req.getParameter("empSer");
		
		EmployeeProperties employeeProperties = new EmployeeProperties();
		employeeProperties.setEmpWorkid(empWorkid);
		employeeProperties.setEmpName(empName);
		employeeProperties.setEmpSex(empSex);
		employeeProperties.setEmpId(empId);
		employeeProperties.setEmpPos(empPos);
		employeeProperties.setEmpSer(empSer);
		try {
			employeeService.insertEmployee(employeeProperties);
			resp.sendRedirect("/hotelmanagesystem/hotel/index");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
   
}
