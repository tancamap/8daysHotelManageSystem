package com.hotelsystem.room.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.room.service.RoomService;


/**
 * @author:lyb
 * @version:
 * @date:2018��6��29������10:36:54
 *
 */
@WebServlet("/hotel/room/Search")
public class RoomSearchServlet extends HttpServlet
{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		req.getRequestDispatcher("/WEB-INF/views/search/RoomSearch.jsp").forward(req, resp);
	}
	RoomService rs = new RoomService();
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");

		String roomNum = req.getParameter("roomNum");
		rs.getRoomById(Integer.parseInt(roomNum));

		resp.sendRedirect("/hotel/rooms");

	}

}
