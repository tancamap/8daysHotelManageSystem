package com.hotelsystem.room.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Room;
import com.hotelsystem.room.service.RoomService;



@WebServlet("/hotel/rooms")
public class RoomsServlet extends HttpServlet {
	
	private RoomService roomService = new RoomService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		List<Room> rooms = roomService.getRooms();

		req.setAttribute("rooms", rooms);
		
		req.getRequestDispatcher("/WEB-INF/views/main/index.jsp").forward(req, resp);
	}

	
}
