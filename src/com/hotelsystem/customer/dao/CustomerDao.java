package com.hotelsystem.customer.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.hotelsystem.common.entity.Customer;
import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.common.utils.DBUtil;

public class CustomerDao {
	private QueryRunner queryRunner=new QueryRunner(DBUtil.getDataSource());
	
	public List<Customer> findAll(){
		String sql="select * from customer";
		try {
			BeanProcessor bean=new BeanProcessor();
			RowProcessor processor=new BasicRowProcessor(bean);
			return queryRunner.query(sql, new BeanListHandler<Customer>(Customer.class, processor));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<Customer> findByRoomNum(int roomnum){
		String sql="select * from customer where roomNum = ?";
		try {
			return queryRunner.query(sql, new BeanListHandler<Customer>(Customer.class), roomnum);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public int delete(int roomnum){
		String sql="delete from customer where roomnum = ?";
		try {
			return queryRunner.update(sql, roomnum);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	public int insert(Customer cus){
		String sql="insert into customer values(?,?,?,?,?,?,?,?)";
		try {
			return queryRunner.update(sql, cus.getCustomerId(),cus.getCarPos(),cus.getRoomNum(),cus.getCustomerName(),cus.getCustomerSex(),cus.isVipMark(),cus.getCustomerPhone(),cus.getCustomerCarId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	public int update(Customer cus,String cusId){
		String sql="update customer set carPos = ?,roomNum = ?,customerName = ?,customerSex = ?,vipMark = ?,customerPhone = ?,customerCarId = ? where customerId = ?";
		try {
			return queryRunner.update(sql, cus.getCarPos(),cus.getRoomNum(),cus.getCustomerName(),cus.getCustomerSex(),cus.isVipMark(),cus.getCustomerPhone(),cus.getCustomerCarId(),cusId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	public Customer findByCustomerId(String cusId){
		String sql="select * from customer where customerId = ?";
		try {
			return queryRunner.query(sql, new BeanListHandler<Customer>(Customer.class), cusId).get(0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/*UpdateRoomState*/
	public int updateRoomState(Room room){
		String sql="update room set roomState = ? where roomNum = ?";
		try {
			return queryRunner.update(sql, room.isRoomState() , room.getRoomNum());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	/*UpdateCarId*/
	public int updateCarId(Park park){
		String sql="update park set carId = ? where carPos = ?";
		try {
			return queryRunner.update(sql, park.getCarId(), park.getCarPos());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
}
