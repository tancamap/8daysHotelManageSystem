package com.hotelsystem.customer.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Customer;
import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.customer.service.CustomerService;

@WebServlet("/hotel/customer/update")
public class UpdateCustomersServlet extends HttpServlet{
	
	private CustomerService customerService=new CustomerService();
	
	private boolean flag=true;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Customer cus=new Customer();
		Room room=new Room();
		Park park=new Park();
		
		room.setRoomNum(Integer.parseInt(req.getParameter("roomNum")));
		room.setRoomState(true);
		park.setCarId(req.getParameter("customerCarId"));
		park.setCarPos(req.getParameter("carPos"));
		
		String cusId=req.getParameter("customerId");
		cus.setCarPos(req.getParameter("carPos"));
		cus.setRoomNum(Integer.parseInt(req.getParameter("roomNum")));
		cus.setCustomerName(req.getParameter("customerName"));
		cus.setCustomerSex(req.getParameter("customerSex"));
		
		String mark=req.getParameter("vipMark");
		if(mark.equals("1") || mark.equals("true") || mark.equals("是")) 
		{
			flag=true;
		}
		else {
			flag=false;
		}
		
		cus.setVipMark(flag);
		cus.setCustomerPhone(req.getParameter("customerPhone"));
		cus.setCustomerCarId(req.getParameter("customerCarId"));
		
		customerService.updateCustomers(cus,cusId);
		customerService.updateRoomStateByCus(room);
		customerService.updateCarIdByCus(park);
		resp.sendRedirect("/hotelmanagesystem/hotel/index");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String cusId=req.getParameter("customerId");
		
		Room room=new Room();
		Park park=new Park();
		
		Customer customer=customerService.getCustomerById(cusId);
		
		room.setRoomNum(customer.getRoomNum());
		room.setRoomState(false);
		park.setCarId("");
		park.setCarPos(customer.getCarPos());
		
		req.setAttribute("customer", customer);
		
		customerService.updateRoomStateByCus(room);
		customerService.updateCarIdByCus(park);
		
		req.getRequestDispatcher("/WEB-INF/views/customer/updatecustomers.jsp").forward(req, resp);
		
	}

	
}
