package com.hotelsystem.manager.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;


import com.hotelsystem.common.utils.*;
import com.hotelsystem.common.entity.Manager;

public class ManagerLoginDao {
     private static QueryRunner queryRunner=new QueryRunner(DBUtil.getDataSource());
     
     public Manager findOne(String username,String password){
    	 String sql="select * from manager where userName = ? and passWord = ?";
    	 try {
			return queryRunner.query(sql,new BeanHandler<Manager>(Manager.class),username,password);
    	 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
    	 }
    	 return null;
     }

}
