package com.hotelsystem.common.entity;

public class Room
{
	private int roomNum;
	private boolean roomState;
	private String roomMold;
	private int roomCost;
	
	public boolean isRoomState()
	{
		return roomState;
	}
	public void setRoomState(boolean roomState)
	{
		this.roomState = roomState;
	}
	public String getRoomMold()
	{
		return roomMold;
	}
	public void setRoomMold(String roomMold)
	{
		this.roomMold = roomMold;
	}
	public int getRoomNum()
	{
		return roomNum;
	}
	public void setRoomNum(int roomNum)
	{
		this.roomNum = roomNum;
	}
	public int getRoomCost() {
		return roomCost;
	}
	public void setRoomCost(int roomCost) {
		this.roomCost = roomCost;
	}
	
	
}
