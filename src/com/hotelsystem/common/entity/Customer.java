package com.hotelsystem.common.entity;

public class Customer {
	private String customerId;
	private String carPos;
	private int roomNum;
	private String customerName;
	private String customerSex;
	private boolean vipMark;
	private String customerPhone;
	private String customerCarId;
	
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCarPos() {
		return carPos;
	}

	public void setCarPos(String carPos) {
		this.carPos = carPos;
	}

	public int getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(int roomNum) {
		this.roomNum = roomNum;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerSex() {
		return customerSex;
	}

	public void setCustomerSex(String customerSex) {
		this.customerSex = customerSex;
	}

	public boolean isVipMark() {
		return vipMark;
	}

	public void setVipMark(boolean vipMark) {
		this.vipMark = vipMark;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerCarId() {
		return customerCarId;
	}

	public void setCustomerCarId(String customerCarId) {
		this.customerCarId = customerCarId;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", carPos=" + carPos + ", roomNum=" + roomNum + ", customerName="
				+ customerName + ", customerSex=" + customerSex + ", vipMark=" + vipMark + ", customerPhone="
				+ customerPhone + ", customerCarId=" + customerCarId + "]";
	}
	
	
}
