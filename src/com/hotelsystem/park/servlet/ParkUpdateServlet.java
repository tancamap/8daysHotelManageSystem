package com.hotelsystem.park.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Park;
import com.hotelsystem.park.dao.ParkDao;
import com.hotelsystem.park.service.ParkService;


@WebServlet("/hotel/park/update")
public class ParkUpdateServlet extends HttpServlet
{
	private ParkService parksService = new ParkService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String carId=req.getParameter("carId");
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		Park park = new Park();
		String carID = req.getParameter("carId");
		String carPos = req.getParameter("carPos");
		park.setCarId(carID);
		park.setCarPos(carPos);

		try {
			parksService.updateCarStatus(park);
			
			resp.sendRedirect("/hotel/rooms");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
